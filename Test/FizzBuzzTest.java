import org.junit.BeforeClass;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FizzBuzzTest {

    List<String> fizzbuzzList;

    @BeforeClass
    public void createList()
    {
        fizzbuzzList = new ArrayList<String>();
    }

    @Test
    public void shouldReturnOneWhenTheNumberisOne() {

        List<String> expctList = new ArrayList<>();
        expctList.add("1");

        FizzBuzz fizzbuzz = new FizzBuzz();
        fizzbuzzList = fizzbuzz.convert(1);

        assertEquals(expctList, fizzbuzzList);

    }

     @Test
    public void shouldReturnTwoWhenTheNumberisTwo() {

         List<String> expctList = Arrays.asList("1", "2");

         FizzBuzz fizzbuzz = new FizzBuzz();

         fizzbuzzList = fizzbuzz.convert(2);
         assertEquals(expctList, fizzbuzzList);

    }

    @Test
    public void shouldReturnFizzWhenTheNumerIsThree() {
        List<String> expctList = Arrays.asList("1", "2", "Fizz");

        FizzBuzz fizzbuzz = new FizzBuzz();

        fizzbuzzList = fizzbuzz.convert(3);
        assertEquals(expctList, fizzbuzzList);
    }

    @Test
    public void shouldReturnFourWhenTheNumberisFour() {
        List<String> expctList = Arrays.asList("1", "2", "Fizz", "4");

        FizzBuzz fizzbuzz = new FizzBuzz();

        fizzbuzzList = fizzbuzz.convert(4);
        assertEquals(expctList, fizzbuzzList);
    }

    @Test
    public void shouldReturnBuzzWhenTheNumerIsFive() {
        List<String> expctList = Arrays.asList("1", "2", "Fizz", "4", "Buzz");

        FizzBuzz fizzbuzz = new FizzBuzz();

        fizzbuzzList = fizzbuzz.convert(5);
        assertEquals(expctList, fizzbuzzList);
    }

    @Test
    public void shouldReturnFizzWhenTheNumerIsSix() {
        List<String> expctList = Arrays.asList("1", "2", "Fizz", "4", "Buzz", "Fizz");

        FizzBuzz fizzbuzz = new FizzBuzz();

        fizzbuzzList = fizzbuzz.convert(6);
        assertEquals(expctList, fizzbuzzList);
    }

    @Test
    public void shouldReturnBuzzWhenTheNumerIsTen() {
        List<String> expctList = Arrays.asList("1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz");

        FizzBuzz fizzbuzz = new FizzBuzz();

        fizzbuzzList = fizzbuzz.convert(10);
        assertEquals(expctList, fizzbuzzList);
    }

    @Test
    public void shouldReturnFizzBuzzWhenTheNumerIsFifteen() {
        List<String> expctList = Arrays.asList("1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz",
                "11", "Fizz", "13", "14", "FizzBuzz");

        FizzBuzz fizzbuzz = new FizzBuzz();

        fizzbuzzList = fizzbuzz.convert(15);
        assertEquals(expctList, fizzbuzzList);
    }

    @Test
    public void shouldReturnEightWhenTheNumerIsEight() {
        List<String> expctList = Arrays.asList("1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8");

        FizzBuzz fizzbuzz = new FizzBuzz();

        fizzbuzzList = fizzbuzz.convert(8);
        assertEquals(expctList, fizzbuzzList);
    }

}