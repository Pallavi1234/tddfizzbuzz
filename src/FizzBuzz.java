import java.util.*;

public class FizzBuzz {
    public List<String> convert(Integer number) {

        List<String> fizzBuzzList = new ArrayList<>();

        for (Integer i = 1; i <= number; i++) {
            if (i % 3 == 0) {
                if (i % 5 == 0)
                    fizzBuzzList.add("FizzBuzz");
                else
                    fizzBuzzList.add("Fizz");
            } else if (i % 5 == 0) {
                fizzBuzzList.add("Buzz");
            } else {
                fizzBuzzList.add(i.toString());
            }
        }

        return fizzBuzzList;
    }
}
